package client

import (
	"context"
	"crypto/tls"
	"fmt"
	"gitee.com/kelvins-io/kelvins/util/client_conn"
	"golang.org/x/net/http2"
	"io/ioutil"
	"net"
	"net/http"
	"sync"
	"testing"
)

var (
	host = "http://%v"
)

func TestClient(t *testing.T) {
	// 服务端支持http2，进行http2调用需要客户端发起协议升级，配置http2 transport
	client := http.Client{
		// Skip TLS dial
		Transport: &http2.Transport{
			AllowHTTP: true,
			DialTLS: func(network, addr string, cfg *tls.Config) (net.Conn, error) {
				return net.Dial(network, addr)
			},
		},
	}
	serviceName := "kelvins-template-http"
	cli, err := client_conn.NewConnClient(serviceName)
	if err != nil {
		t.Error(err)
		return
	}
	endpoints, err := cli.GetEndpoints(context.TODO())
	if err != nil {
		t.Error(err)
		return
	}
	fn := func() {
		for i := 0; i < len(endpoints); i++ {
			path := fmt.Sprintf(host, endpoints[i]) + "/hello"
			req, err := http.NewRequest(http.MethodGet, path, nil)
			if err != nil {
				t.Error(err)
				continue
			}
			resp, err := client.Do(req)
			if err != nil {
				t.Error(err)
				continue
			}
			t.Logf("%v response status %v\n", path, resp.Status)
			headerResp := resp.Header
			for i, v := range headerResp {
				t.Logf("%v=%v\n", i, v)
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
				continue
			}
			err = resp.Body.Close()
			if err != nil {
				continue
			}
			t.Logf("%v response body %v\n", path, string(body))
			return
		}
	}
	wg := sync.WaitGroup{}
	concurrent := 10
	wg.Add(concurrent)
	for i := 0; i < concurrent; i++ {
		go func() {
			defer wg.Done()
			fn()
		}()
	}
	wg.Wait()
}

func TestRateLimit(t *testing.T) {
	client := http.Client{
		Transport: &http2.Transport{
			AllowHTTP: true,
			DialTLS: func(network, addr string, cfg *tls.Config) (net.Conn, error) {
				return net.Dial(network, addr)
			},
		},
	}
	serviceName := "kelvins-template-http"
	cli, err := client_conn.NewConnClient(serviceName)
	if err != nil {
		t.Error(err)
		return
	}
	endpoints, err := cli.GetEndpoints(context.TODO())
	if err != nil {
		t.Error(err)
		return
	}
	fn := func() {
		for i := 0; i < len(endpoints); i++ {
			path := fmt.Sprintf(host, endpoints[i]) + "/hello"
			req, err := http.NewRequest(http.MethodGet, path, nil)
			if err != nil {
				t.Error(err)
				continue
			}
			resp, err := client.Do(req)
			if err != nil {
				t.Error(err)
				continue
			}
			t.Logf("%v response status %v\n", path, resp.Status)
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
				continue
			}
			err = resp.Body.Close()
			if err != nil {
				continue
			}
			t.Logf("%v response body %v\n", path, string(body))
			return
		}
	}
	wg := sync.WaitGroup{}
	concurrent := 10
	wg.Add(concurrent)
	for i := 0; i < concurrent; i++ {
		go func() {
			defer wg.Done()
			fn()
		}()
	}
	wg.Wait()
}
