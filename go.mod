module gitee.com/kelvins-io/kelvins-template-http

go 1.13

require (
	gitee.com/kelvins-io/common v1.1.5
	gitee.com/kelvins-io/kelvins v1.6.1
	github.com/gin-gonic/gin v1.7.1
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974
	google.golang.org/grpc v1.40.0
)
