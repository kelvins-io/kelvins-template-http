package main

import (
	"gitee.com/kelvins-io/kelvins"
	"gitee.com/kelvins-io/kelvins-template-http/router"
	"gitee.com/kelvins-io/kelvins-template-http/startup"
	"gitee.com/kelvins-io/kelvins/app"
)

const APP_NAME = "kelvins-template-http"

func main() {
	application := &kelvins.HTTPApplication{
		Application: &kelvins.Application{
			LoadConfig: startup.LoadConfig,
			SetupVars:  startup.SetupVars,
			StopFunc:   startup.StopFunc,
			Name:       APP_NAME,
		},
		RegisterHttpGinRoute: router.RegisterHttpGinRoute,
	}
	app.RunHTTPApplication(application)
}
