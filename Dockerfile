FROM golang:1.13.15-buster as gobuild
WORKDIR $GOPATH/src/gitee.com/kelvins-io/kelvins-template-http
COPY . .
ENV GOPROXY=https://goproxy.io,direct
RUN bash ./build.sh
# FROM alpine:latest as gorun
FROM ubuntu:latest as gorun
WORKDIR /www/
COPY --from=gobuild /go/src/gitee.com/kelvins-io/kelvins-template-http/kelvins-template-http .
COPY --from=gobuild /go/src/gitee.com/kelvins-io/kelvins-template-http/etc ./etc
CMD ["./kelvins-template-http"]
