package startup

import (
	"gitee.com/kelvins-io/kelvins-template-http/vars"
	"gitee.com/kelvins-io/kelvins/config"
)

const (
	SectionEmailConfig = "email-config"
	SectionEmailNotice = "email-notice"
)

// LoadConfig 加载配置对象映射
func LoadConfig() error {
	// 举例说明
	vars.EmailConfigSetting = new(vars.EmailConfigSettingS)
	config.MapConfig(SectionEmailConfig, vars.EmailConfigSetting)

	vars.EmailNoticeSetting = new(vars.EmailNoticeSettingS)
	config.MapConfig(SectionEmailNotice, vars.EmailNoticeSetting)
	return nil
}
