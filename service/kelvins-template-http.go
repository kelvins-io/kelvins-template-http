package service

import (
	"context"
	"gitee.com/kelvins-io/kelvins"
	"gitee.com/kelvins-io/kelvins/util/queue_helper"
	"time"
)

func Hello() {
	go func() {
		pushHelper, err := queue_helper.NewPublishService(kelvins.QueueServerAMQP, &queue_helper.PushMsgTag{
			DeliveryTag:    "kelvins-template_notice",
			DeliveryErrTag: "kelvins-template_notice_err",
			RetryCount:     2,
			RetryTimeout:   3600,
		}, kelvins.BusinessLogger)
		if err == nil {
			type notice struct {
				Service string
				Time    int64
			}
			_, _ = pushHelper.PushMessage(context.TODO(), &notice{
				Service: "service1",
				Time:    time.Now().Unix(),
			})
		}
	}()
	time.Sleep(1 * time.Second)
}
