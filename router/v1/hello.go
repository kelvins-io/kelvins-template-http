package v1

import (
	"fmt"
	"gitee.com/kelvins-io/kelvins-template-http/service"
	"gitee.com/kelvins-io/kelvins/util/gin_helper"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Hello(c *gin.Context) {
	service.Hello()
	requestId := gin_helper.GetRequestId(c)
	s := fmt.Sprintf("client ip=%v,requestId=%v, uri=%v", c.ClientIP(), requestId, c.Request.RequestURI)
	gin_helper.JsonResponse(c, http.StatusOK, gin_helper.SUCCESS, s)
}
