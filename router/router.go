package router

import (
	"gitee.com/kelvins-io/kelvins-template-http/router/v1"
	"github.com/gin-gonic/gin"
)

func RegisterHttpGinRoute(r *gin.Engine) {
	r.GET("/hello", v1.Hello)
}
